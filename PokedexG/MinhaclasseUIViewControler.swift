//
//  MinhaclasseUIViewControler.swift
//  PokedexG
//
//  Created by COTEMIG on 31/03/22.
//

class MyCell: UITableViewCell{
    @IBOutlet weak var label: UITableView!
}


import UIKit

private let reuseIdentifier = "Cell"



class ListaViewController: UIViewController, UITableViewDataSource{
    
        @IBOutlet weak var tableview: UITableView!
        override func viewDidLoad() {
            super.viewDidLoad()
            tableview.dataSource = self
    
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10 // #1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as? MyCell {cell.label.text = "LinhaAtual: \(indexPath.row)"
            return cell
        } // 4#
        return UITableViewCell()
    }
 
}

