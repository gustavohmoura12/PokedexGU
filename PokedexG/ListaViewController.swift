//
//  MinhaclasseUIViewControler.swift
//  PokedexG
//
//  Created by COTEMIG on 31/03/22.
//



import UIKit

struct Pokemon {
    let nome: String
    let tipo1: String
    let tipo2: String
}


class ListaViewController: UIViewController{
    
    @IBOutlet weak var tableview: UITableView!
    var listaDePokemons: [Pokemon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.dataSource = self
        listaDePokemons.append(Pokemon(nome: "Bulbassauro",
                                       tipo1: "Plata",
                                       tipo2: "Veneno"
                                       ))
        listaDePokemons.append(Pokemon(nome: "Ivyssaur",
                                       tipo1: "Plata",
                                       tipo2: "Veneno"
                                       ))
        tableview.reloadData()
        
    }
    
    
 
}
extension ListaViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return listaDePokemons.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as? MyCell {
          let Pokemon = listaDePokemons[indexPath.row]
            cell.nome.text = Pokemon.nome
            cell.tipo1.text = Pokemon.tipo1
            cell.tipo2.text = Pokemon.tipo2
            return cell
        } // 4#
        return UITableViewCell()
    }
}
